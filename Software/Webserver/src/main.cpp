/*
    Name: Robotic Hand Webserver    Author: MartinaH     Date: March 2019
    This program is based on example PathArgServer from espressif github:
    https://github.com/espressif/arduino-esp32/blob/master/libraries/WebServer/examples/PathArgServer/PathArgServer.ino
    Others sources: Arduino libraries: WiFi.h, WiFiClient.h, WebServer.h, ESPmDNS.h, AdafruitNeoPixel.h, ESP32Servo.h
*/

// Internal Arduino libraries
#include <WiFi.h>
#include <WiFiClient.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <Adafruit_NeoPixel.h>
#include <ESP32Servo.h>

// RGB LED ring initiation
Adafruit_NeoPixel ring = Adafruit_NeoPixel(16, 4, NEO_GRB + NEO_KHZ800);

// Replace with your SSID and password
const char *ssid = "-----";
const char *password = "----";

extern const char *web;
extern const char *ruka;

// List of functions
void handleNotFound();
void WebRoot();
void ServoValue();
void RingValue();

WebServer server(80);

// Making servo objects
Servo palec;
Servo ukazovak;
Servo prostrednik;
Servo prstenik;
Servo malik;
Servo zakladna;

String battery;

void WebRoot()
{
    server.send(200, "text/html", web);
}

// Getting values from slider bars on website
void ServoValue()
{
   Serial.printf("Servo: %s val: %s\n", server.pathArg(0).c_str(), server.pathArg(1).c_str());

   String request1 = server.pathArg(0).c_str(); 
   String request2 = server.pathArg(1).c_str();

   int servo = request1.toInt(); // Slider bar number
   int pos = request2.toInt();   // Slider bar position

   // Sending positions to servos
   switch(servo)
   {
       case 1: palec.write(pos); delay(20);
        break;
       case 2: ukazovak.write(pos); delay(20);
        break;
       case 3: prostrednik.write(pos); delay(20);
        break;
       case 4: prstenik.write(pos); delay(20);
        break;
       case 5: malik.write(pos); delay(20);
        break;
       case 6: zakladna.write(pos); delay(20);
        break;
   }
   server.send(200, "text/html", "Hello here");
}

// Getting LED number and RGB code from website
void RingValue()
{    
   Serial.printf("LED: %s red: %s green: %s blue: %s\n", server.pathArg(0).c_str(), server.pathArg(1).c_str(), server.pathArg(2).c_str(), server.pathArg(3).c_str());
   String request1 = server.pathArg(0).c_str();
   String request2 = server.pathArg(1).c_str();
   String request3 = server.pathArg(2).c_str();
   String request4 = server.pathArg(3).c_str();
   
   int ledNumber = request1.toInt();
   int red = request2.toInt();
   int green = request3.toInt();
   int blue = request4.toInt();

   // Sending values to RGB LED ring
   ring.setPixelColor(ledNumber-1, ring.Color(red, green, blue)); 
   ring.show();
    
   server.send(200, "text/html", "Hello here");
}

void setup(void)
{
    Serial.begin(115200);

    palec.attach(25);
    ukazovak.attach(26);
    prostrednik.attach(27);
    prstenik.attach(14);
    malik.attach(12);
    zakladna.attach(13);

    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    Serial.println("");
    
    // Uncoloring RGB LED ring
    ring.begin();
    for(int i = 0; i <=15; i++)
    {
        ring.setPixelColor(i, ring.Color(0, 0, 0)); 
        ring.show();
    }
    
    // Waiting for connection
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    if (MDNS.begin("ruka")) {
        Serial.println("MDNS responder started");
    }   
    
    // Getting values from website
    server.on("/", WebRoot);
    server.on("/servo/{}/{}", ServoValue);
    server.on("/ring/{}/{}/{}/{}", RingValue);
    
    // Sending data on website (battery state and image of robotic hand)
    server.on("/battery", [](){ server.send(200, "text/plain", String(map(analogRead(33), 0, 1024, 0, 4200))); });
    server.on("/ruka.jpg", [](){ server.send_P(200, "img/jpg", ruka, 43078); });

    server.onNotFound(handleNotFound);

    server.begin();
    Serial.println("HTTP server started");
  
}

void loop(void) {
    server.handleClient();
}

void handleNotFound() 
{
    String message = "File Not Found\n\n";
    message += "URI: ";
    message += server.uri();
    message += "\nMethod: ";
    message += (server.method() == HTTP_GET) ? "GET" : "POST";
    message += "\nArguments: ";
    message += server.args();
    message += "\n";
    for (uint8_t i = 0; i < server.args(); i++) {
        message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
    }
    server.send(404, "text/plain", message);
}
