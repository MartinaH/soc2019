/* 
Name: LSM303D.h    Author: MartinaH    Date: March 2019
Description: LSM303D accelerometer library
*/

#define WHO_AM_I 0x0F
#define READ_REG 1<<7 
#define AUTO_INC 1<<6

#define TEMP_OUT_L 0x05
#define TEMP_OUT_H 0x06
#define CTRL1 0x20
#define CTRL2 0x21
#define CTRL5 0x24
#define CTRL7 0x26
#define STATUS_A 0x27
#define OUT_X_L_A 0x28
#define OUT_X_H_A 0x29
#define OUT_Y_L_A 0x2A
#define OUT_Y_H_A 0x2B
#define OUT_Z_L_A 0x2C
#define OUT_Z_H_A 0x2D


#define CTRL1_DR_3Hz (0b0001 <<4)
#define CTRL1_DR_6Hz (0b0010 <<4)
#define CTRL1_DR_12Hz (0b0011 <<4)
#define CTRL1_DR_25Hz (0b0100 <<4)
#define CTRL1_DR_50Hz (0b0101 <<4)
#define CTRL1_DR_100Hz (0b0110 <<4)
#define CTRL1_DR_200Hz (0b0111 <<4)
#define CTRL1_DR_400Hz (0b1000 <<4)
#define CTRL1_DR_800Hz (0b1001 <<4)
#define CTRL1_DR_1600Hz (0b1010 <<4)
#define CTRL1_EN 0b111

#define CTRL2_ABW_773Hz (0b00<<6)
#define CTRL2_ABW_194Hz (0b01<<6)
#define CTRL2_ABW_326Hz (0b10<<6)
#define CTRL2_ABW_50Hz (0b11<<6)

#define CTRL2_AFS_2g (0b000<<3)
#define CTRL2_AFS_4g (0b001<<3)
#define CTRL2_AFS_6g (0b010<<3)
#define CTRL2_AFS_8g (0b011<<3)
#define CTRL2_AFS_16g (0b100<<3)

#define CTRL5_TEMP_EN (1<<7)
#define CTRL5_M_ODR (001<<2)
#define CTRL7_T_ONLY (1<<4)

