/* 
Name: main.h    Author: MartinaH    Date: March 2019
Description: ESP32 pins definitions library
*/

#define CS0_OFF  digitalWrite(5, HIGH)
#define CS0_ON  digitalWrite(5, LOW)

#define CS1_OFF  digitalWrite(4, HIGH)
#define CS1_ON   digitalWrite(4, LOW)

#define CS2_OFF  digitalWrite(0, HIGH)
#define CS2_ON  digitalWrite(0, LOW)

#define CS3_OFF  digitalWrite(2, HIGH)
#define CS3_ON   digitalWrite(2, LOW)

#define CS4_OFF  digitalWrite(15, HIGH)
#define CS4_ON  digitalWrite(15, LOW)

#define AllCS_ON  {CS0_ON; CS1_ON; CS2_ON; CS3_ON; CS4_ON;}
#define AllCS_OFF {CS0_OFF; CS1_OFF; CS2_OFF; CS3_OFF; CS4_OFF;}

#define LED_ON   digitalWrite(32, HIGH)
#define LED_OFF  digitalWrite(32, LOW)

#define VBAT 33

void CheckBattery();
void CS_ON(int num);


