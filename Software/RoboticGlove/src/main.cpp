/*
    Name: Robotic Glove     Author: MartinaH     Date: March 2019
    This program is based on example of accespoint - station communication from Gyalu1: 
    https://www.instructables.com/id/Accesspoint-Station-Communication-Between-Two-ESP8/
    Others sources: internal Arduino libraries: SPI.h, WiFi.h, Wire.h
*/

// Internal Arduino libraries
#include <Arduino.h>    
#include <SPI.h>
#include <WiFi.h>
#include <Wire.h>

// My libraries
#include "LM303D.h" // Library for LSM303D accelerometer
#include "main.h"   // Library with ESP32 pins definitions

const int MPU_addr=0x68; // MPU6050 address

SPIClass * vspi = NULL; 
static const int spiClk = 1000000; // 1 MHz

int16_t ACC_data, ACC_Z[5], GyZ;  

char ssid[] = "Wemos_AP";           // SSID of your AP
char pass[] = "Wemos_comm";         // password of your AP

IPAddress server(192,168,4,15);     // IP address of the AP
WiFiClient client;

unsigned long actualTime, preTime;

void setup()
{
  // I2C initiation for MPU6050
  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);
 
  Serial.begin(115200);

  // ESP32 pin directions
  pinMode(5, OUTPUT); // CS0
  pinMode(4, OUTPUT); // CS1
  pinMode(0, OUTPUT); // CS2
  pinMode(2, OUTPUT); // CS3
  pinMode(15, OUTPUT); // CS4
  pinMode(32, OUTPUT); // LED
  pinMode(33, INPUT); // battery 
  
  // SPI initiation for LSM303D
  vspi = new SPIClass(VSPI);
  vspi->begin(); 
  vspi->beginTransaction(SPISettings(spiClk, MSBFIRST, SPI_MODE3));
  
  for(int ii = 0; ii < 2; ii++)
  {
     for(int i = 0; i < 5; i++)
    {
      CS_ON(i); vspi->transfer(CTRL1); vspi->transfer(CTRL1_EN | CTRL1_DR_50Hz); AllCS_OFF; 
      CS_ON(i); vspi->transfer(CTRL2); vspi->transfer(CTRL2_ABW_194Hz); AllCS_OFF; 
    }
  }

  CheckBattery();
  
  // WiFi initiation
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);           
  Serial.println();
  Serial.println("Connection to the AP");

  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  Serial.println();
  Serial.println("Robotic Glove");
  Serial.println("Connected");
  Serial.print("LocalIP:"); Serial.println(WiFi.localIP());
  Serial.println("MAC:" + WiFi.macAddress());
  Serial.print("Gateway:"); Serial.println(WiFi.gatewayIP());
  Serial.print("AP MAC:"); Serial.println(WiFi.BSSIDstr());
}

void loop()
{
  // Reading gyro Z axis value from MPU6050
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x47);  
  Wire.endTransmission(false); 
  Wire.requestFrom(MPU_addr,2,true);  
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)

  // Reading accelerometers Z axis value from LSM303D
  for(int n = 0; n < 5; n++)
  {
    CS_ON(n); vspi->transfer(READ_REG | AUTO_INC | OUT_Z_L_A); 
    ACC_data = vspi->transfer16(0x00); AllCS_OFF;
    ACC_Z[n] = ((uint8_t)ACC_data << 8) | (uint8_t)(ACC_data >> 8);  
    ACC_Z[n] *= 0.0610; 
  }
  
  // Connecting to the AP
  client.connect(server, 80);

  // Converting data to string
  String pos  = String(ACC_Z[0]);
  String pos2 = String(ACC_Z[1]);
  String pos3 = String(ACC_Z[2]);
  String pos4 = String(ACC_Z[3]);
  String pos5 = String(ACC_Z[4]);
  String pos6 = String(GyZ);
  
  // Printing data on serial monitor
  Serial.println("********************************");
  Serial.println("Data sent to the AP: ");
  Serial.print(pos + "/t" + pos2 + "/t" + pos3 + "/t" + pos4 + "/t" + pos5 + "/t" + pos6 + "/t");

  // Sending data to AP
  client.print(pos+"\r");
  client.print(pos2+"\r");
  client.print(pos3+"\r");
  client.print(pos4+"\r");
  client.print(pos5+"\r");
  client.print(pos6+"\r");
  delay(500);
  client.flush();
  client.stop();

  // Checking the battery state every minute
  actualTime = millis();
  if((actualTime - preTime) > 60000)
  {
      preTime = actualTime;
      CheckBattery();
  }
}

void CheckBattery()
{
  int battery = 0;

  for(int i = 0; i < 20; i++)
  {
    battery += analogRead(33);
  }
  battery /= 20; // Average of 20 values

  int milivolts = battery * 4.07; // Conversion ADC to mV
  
  Serial.print("Battery: ");
  Serial.print(milivolts);
  Serial.println(" mV");

  int bat_level = 0;
  
       if(milivolts < 3000) LED_ON;
  else if(milivolts < 3200) bat_level = 1;
  else if(milivolts < 3400) bat_level = 2;
  else if(milivolts < 3600) bat_level = 3;
  else if(milivolts < 3800) bat_level = 4;
  else if(milivolts < 4000) bat_level = 5;
  else                      bat_level = 6;
  
  // The number of red LED flashes corresponds to the battery voltage
  for(int i = 0; i < bat_level; i++) {LED_ON; delay(200); LED_OFF; delay(200);}
}

// This function turning on selected chip select
void CS_ON(int num)
{
  switch(num)
   {
     case 0: CS0_ON; break;
     case 1: CS1_ON; break;
     case 2: CS2_ON; break;
     case 3: CS3_ON; break;
     case 4: CS4_ON; break;
   }
}