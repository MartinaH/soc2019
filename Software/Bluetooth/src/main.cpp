/*
    Name: Robotic Hand BT     Author: MartinaH     Date: March 2019
    This program is based on example of accespoint - station communication from Gyalu1: 
    https://circuitdigest.com/microcontroller-projects/using-classic-bluetooth-in-esp32-and-toogle-an-led
    Others sources: internal Arduino libraries: BluetoothSerial.h, ESP32Servo.h
*/

// Internal Arduino libraries
#include <BluetoothSerial.h> 
#include <ESP32Servo.h>

// Making servo objects
Servo thumb;
Servo indexFinger;
Servo middleFinger;
Servo ringFinger;
Servo smallFinger;
Servo wrist;

// Making Bluetooth object
BluetoothSerial ESP_BT; 

int incoming[6];

void setup() {
  Serial.begin(115200); 
  ESP_BT.begin("RoboRuka"); 
  Serial.println("Bluetooth Device is Ready to Pair");
  
  thumb.attach(25);
  indexFinger.attach(26);
  middleFinger.attach(27);
  ringFinger.attach(14);
  smallFinger.attach(12);
  wrist.attach(13);
}

void loop() {
  
  if (ESP_BT.available()) //Check if we receive anything from Bluetooth
  {
    // Reading 6 bytes
    for(int i = 0; i < 6; i++)
    {
        incoming[i] = ESP_BT.read(); 
        Serial.println(incoming[i]);
    }

    if (incoming[0] == 244 && incoming[1] == 1) {thumb.write(incoming[4]);}
    if (incoming[0] == 244 && incoming[1] == 2) {ringFinger.write(incoming[4]);}
    if (incoming[0] == 244 && incoming[1] == 3) {middleFinger.write(incoming[4]);}
    if (incoming[0] == 244 && incoming[1] == 4) {ringFinger.write(incoming[4]);}
    if (incoming[0] == 244 && incoming[1] == 5) {smallFinger.write(incoming[4]);}
    if (incoming[0] == 244 && incoming[1] == 6) {wrist.write(incoming[4]);} 
  }
  delay(100);
}
