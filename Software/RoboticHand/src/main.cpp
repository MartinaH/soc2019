/*
    Name: Robotic Hand     Author: MartinaH     Date: March 2019
    This program is based on example of accespoint - station communication from Gyalu1: 
    https://www.instructables.com/id/Accesspoint-Station-Communication-Between-Two-ESP8/
    Others sources: internal Arduino libraries: AdafruitNeopixel.h, WiFi.h, ESP32Servo.h
*/

// Internal Arduino libraries
#include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <WiFi.h>
#include <ESP32Servo.h>

// Making servo objects
Servo thumb;
Servo indexFinger;
Servo middleFinger;
Servo ringFinger;
Servo smallFinger;
Servo wrist;

#define VBAT 33

WiFiServer server(80);
Adafruit_NeoPixel ring = Adafruit_NeoPixel(16, 4, NEO_GRB + NEO_KHZ800);
IPAddress IP(192,168,4,15);
IPAddress mask (255, 255, 255, 0);

void CheckBattery();

void setup() {
  thumb.attach(25);
  indexFinger.attach(26);
  middleFinger.attach(27);
  ringFinger.attach(14);
  smallFinger.attach(12);
  wrist.attach(13);
  
  pinMode(VBAT, INPUT);

  Serial.begin(115200);
  ring.begin();
  
  CheckBattery();

  // Starting client server
  WiFi.mode(WIFI_AP);
  WiFi.softAP("Wemos_AP", "Wemos_comm");
  WiFi.softAPConfig(IP, IP, mask);
  server.begin();
 
  Serial.println();
  Serial.println("Robotic Hand");
  Serial.println("Server started.");
  Serial.print("IP: ");     Serial.println(WiFi.softAPIP());
  Serial.print("MAC:");     Serial.println(WiFi.softAPmacAddress());
}

long Millis, currentTime = 0;

void loop() {
  WiFiClient client = server.available();
  if (!client) {return;}
  // Reading data from the station
  String request = client.readStringUntil('\r');
  String request2 = client.readStringUntil('\r');
  String request3 = client.readStringUntil('\r');
  String request4 = client.readStringUntil('\r');
  String request5 = client.readStringUntil('\r');
  String request6 = client.readStringUntil('\r');

  // Printing data on serial monitor
  Serial.println("********************************");
  Serial.println("From the station: ");
  Serial.println("1." + request);
  Serial.println("2." + request2);
  Serial.println("3." + request3);
  Serial.println("4." + request4);
  Serial.println("5." + request5);
  Serial.println("6." + request6);
 
  client.flush();
  
  // Converting the accelerometer and gyroscope positions to the servo positions
  int pos1 = map(request.toInt(), -900, 900, 0, 180);
  int pos2 = map(request2.toInt(), -900, 900, 0, 180);
  int pos3 = map(request3.toInt(), -900, 900, 0, 180);
  int pos4 = map(request4.toInt(), -900, 900, 0, 180);
  int pos5 = map(request5.toInt(), -900, 900, 0, 180);
  int pos6 = map(request6.toInt(), -200, 2800, 0, 180);

  // Sending positions to servos
  thumb.write(pos1);
  indexFinger.write(pos2);
  middleFinger.write(pos3);
  ringFinger.write(pos4);
  smallFinger.write(pos5);
  wrist.write(pos6);

  delay(300);

  // Checking the battery state every minute
  Millis = millis();
  if((Millis - currentTime) > 60000)
  {
      currentTime = Millis;
      CheckBattery();
  }
  
}

void CheckBattery()
{
   int red = 0, green = 0, blue = 0;
   int battery = 0;

   for(int i = 0; i < 20; i++)
   {
     battery += analogRead(33);
   }
   battery /= 20; // Average of 20 values
   battery *= 4.07; // Conversion ADC to mV

   // The RGB LED ring color corresponds to the battery voltage
   if(battery < 3000) {red = 255; green = 0; blue = 0;}
   else if(battery < 3200) {red = 200; green = 0; blue = 255;}
   else if(battery < 3400) {red = 255; green = 0; blue = 150;}
   else if(battery < 3600) {red = 255; green = 255; blue = 255;}
   else if(battery < 3800) {red = 0; green = 0; blue = 255;}
   else if(battery < 4000) {red = 0; green = 255; blue = 255;}
   else {red = 0; green = 255; blue = 0;}

   for(int i=0;i<=16;i++)
   {
     ring.setPixelColor(i, ring.Color(red, green, blue)); 
     ring.show();
   }
   Serial.printf("Battery: %i mV\n", battery);
}